#define _USE_MATH_DEFINES

#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include <string>
#include <iomanip>
#include <numeric>

#include "Curves.h"

int main() {

	std::vector<std::shared_ptr<Curve>> curves;//first container
	std::vector<std::shared_ptr<Circle>> circles;//second container

	std::random_device randomDevice;
	std::mt19937 gen(randomDevice());
	std::uniform_real_distribution<double> double_dist(0.1, 5.);
	std::uniform_int_distribution<short> short_dist(0, 2);

	//populating first container randomly
	int num_curves = 100;
	for (int i = 0; i < num_curves; ++i) {
		switch (short_dist(gen)) {
		case 0: curves.push_back(std::make_shared<Circle>(double_dist(gen))); break;
		case 1: curves.push_back(std::make_shared<Ellipse>(double_dist(gen), double_dist(gen))); break;
		default: curves.push_back(std::make_shared<Helix>(double_dist(gen), double_dist(gen))); break;
		}
	}

	//t = PI / 4
	double t = M_PI_4;
	
	//printing first container curves info with parameter t
	std::string curveTitle;
	std::cout << "All curves: " << std::endl;
	for (auto curve : curves) {
		if (std::dynamic_pointer_cast<Helix>(curve)) {
			curveTitle = "Helix";
		} else if (std::dynamic_pointer_cast<Circle>(curve)) {
			curveTitle = "Circle";
			circles.push_back(std::dynamic_pointer_cast<Circle>(curve));//populating second container with circles
		} else {
			curveTitle = "Ellipse";
		}
		std::cout << std::left << std::setw(14) << (curveTitle + ": ") << "point=" << curve->getPoint(t) << "        derivative=" << curve->getFirstDerivative(t) << std::endl;
	}
	std::cout << std::endl;
	curves.clear();
	
	//sorting circles by radius
	std::sort(circles.begin(), circles.end(), [](const std::shared_ptr<Circle>& a, const std::shared_ptr<Circle>& b) { return (*a) < (*b); });
	
	//printing second container info with parameter t
	std::cout << "Sorted circles: " << std::endl;
	for (const auto& circle : circles) {
		std::cout << std::left << std::setw(14) << ("Circle: ") << "point=" << (circle->getPoint(t)) << "        derivative=" << (circle->getFirstDerivative(t)) << std::endl;
	}
	std::cout << std::endl;
	

	//circles radii sum
	const double radii_sum = std::accumulate(std::cbegin(circles), std::cend(circles), 1., [](double sum, const std::shared_ptr<Circle>& circle) {return sum + circle->getRadius(); });
	std::cout << "Circles radii sum: " << radii_sum << std::endl;
	
	std::cout << "Press Enter to exit" << std::endl;
	std::cin.get();

	return 0;
}