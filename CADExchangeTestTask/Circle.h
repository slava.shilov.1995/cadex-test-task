#pragma once

#include <cmath>
#include <optional>

#include "Curve.h"

class Circle : public Curve {
private:
	double radius;
public:
	Circle(double radius) {
		if (radius < 0) {
			throw std::runtime_error("Negative number");
		}
		this->radius = radius;
	};

	Vector3 getPoint(const double t) const {
		return Vector3(cos(t) * radius, sin(t) * radius, 0.);
	}

	Vector3 getFirstDerivative(const double t) const {
		return Vector3(-sin(t) * radius, cos(t) * radius, 0);
	}

	bool operator<(const Circle& rhs) const {
		return radius < rhs.radius;
	}

	double getRadius() const {
		return radius;
	}
};