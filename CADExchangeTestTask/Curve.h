#pragma once

#include "Vector3.h"

class Curve {
public:
	Curve() = default;
	virtual ~Curve() = default;
	virtual Vector3 getPoint(const double t) const = 0;
	virtual Vector3 getFirstDerivative(const double t) const = 0;
};