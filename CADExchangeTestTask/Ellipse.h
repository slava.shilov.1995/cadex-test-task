#pragma once

#include "Curve.h"

class Ellipse : public Curve {
private:
	double radiusX, radiusY;
public:
	Ellipse(double radiusX, double radiusY) {
		if (radiusX < 0 || radiusY < 0) {
			throw std::runtime_error("Negative number");
		}
		this->radiusX = radiusX;
		this->radiusY = radiusY;
	}

	Vector3 getPoint(const double t) const {
		return Vector3(cos(t) * radiusX, sin(t) * radiusY, 0.);
	}

	Vector3 getFirstDerivative(const double t) const {
		return Vector3(-sin(t) * radiusX, cos(t) * radiusY, 0);
	}
};