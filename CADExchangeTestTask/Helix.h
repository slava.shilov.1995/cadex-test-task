#pragma once
#define _USE_MATH_DEFINES

#include <cmath>
#include "Curve.h"

class Helix : public Curve {
	static const double pi2;
private:
	double radius, step;
public:
	Helix(double radius, double step) {
		if (radius < 0) {
			throw std::runtime_error("Negative radius");
		}
		this->radius = radius;
		this->step = step;
	}

	Vector3 getPoint(const double t) const {
		return Vector3(cos(t) * radius, sin(t) * radius, step * (t / pi2));
	}

	Vector3 getFirstDerivative(const double t) const {
		return Vector3(-sin(t) * radius, cos(t) * radius, step / pi2);
	}
};

const double Helix::pi2 = 6.283185307179586476925286766559;