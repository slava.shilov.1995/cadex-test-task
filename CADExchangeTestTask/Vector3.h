#pragma once

#include <iostream>
#include <iomanip>

struct Vector3 {
	double x, y, z;
	Vector3(double x, double y, double z) : x(x), y(y), z(z) {};
};

std::ostream& operator<<(std::ostream& s, const Vector3 p) {
	return s << "( " << std::setw(9) << p.x << ", " << std::setw(9) << p.y << ", " << std::setw(9) << p.z << ")";
}